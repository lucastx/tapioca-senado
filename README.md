# Tapioca Senado

## Installation
```
pip install tapioca-senado
```

## Documentation
``` python
from tapioca_senado import Senado


api = Senado()

```

No more documentation needed.

- Learn how Tapioca works [here](http://tapioca-wrapper.readthedocs.org/en/stable/quickstart.html)
- Explore this package using iPython
- Have fun!

